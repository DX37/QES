#ifndef QUAD_EQU_H
#define QUAD_EQU_H

typedef struct
{
    double d;
    double x1;
    double x2;
} Coef;

Coef quadEqu(double a, double b, double c);

#endif
