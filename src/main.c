#include <stdio.h>
#include <locale.h>
#include "quad_equ.h"

int main()
{
    setlocale(LC_ALL, "Russian");
    double a, b, c;
    Coef result;
    printf("==== ax^2+bx+c=0 ====\n");
    printf("Введите коэффициенты a, b, c: ");
    scanf("%lf %lf %lf", &a, &b, &c);

    if (a == 0)
    {
        printf("Коэффициент a должен быть не равен нулю.\n");
        return -1;
    }
    
    result = quadEqu(a, b, c);

    printf("D: %.2lf.\n", result.d);
    
    if (result.d < 0)
        printf("Корней нет.\n");
    else if (result.d == 0)
        printf("x = %.2lf.\n", result.x1);
    else
        printf("x1: %.2lf. x2: %.2lf.\n", result.x1, result.x2);

    return 0;
}
