#include <stdio.h>
#include <math.h>
#include "quad_equ.h"

Coef quadEqu(double a, double b, double c)
{
    Coef equation;
    if (a == 0)
    {
        equation.x1 = equation.x2 = 0;
        return equation;
    }
    equation.d = b * b - 4 * a * c;

    if (equation.d < 0)
        return equation;
    
    if (equation.d == 0)
    {
        equation.x1 = equation.x2 = -(b / (2 * a));
        return equation;
    }

    equation.x1 = (-b + sqrt(equation.d)) / (2 * a);
    equation.x2 = (-b - sqrt(equation.d)) / (2 * a);
    return equation;
}
