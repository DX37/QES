.PHONY: clean test prepare

#############

bin/quadEqu: prepare obj/main.o obj/quad_equ.o
	gcc -Wall -o bin/quadEqu obj/main.o obj/quad_equ.o -lm

obj/main.o: src/main.c
	gcc -Wall -c src/main.c -o obj/main.o

obj/quad_equ.o: src/quad_equ.c
	gcc -Wall -c src/quad_equ.c -o obj/quad_equ.o

#############

bin/test: prepare obj/quad_equ.o obj/quad_equ_test.o obj/ctest.o
	gcc -Wall -o bin/test obj/quad_equ.o obj/quad_equ_test.o obj/ctest.o -lm

obj/ctest.o: test/ctest.c
	gcc -Wall -c test/ctest.c -o obj/ctest.o -Ithirdparty

obj/quad_equ_test.o: test/quad_equ_test.c
	gcc -Wall -c test/quad_equ_test.c -o obj/quad_equ_test.o -Ithirdparty -Isrc

#############

prepare: bin obj

bin:
	mkdir bin

obj:
	mkdir obj

app: bin/quadEqu
	bin/quadEqu

test: bin/test
	bin/test

clean:
	rm -f bin/* obj/*.o
